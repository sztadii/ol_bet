var info;

info = new function () {

  //catch DOM
  var $el;
  var $close;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".info");
    $close = $el.find('.info__close');

    watchClose();
  };

  var watchClose = function () {
    $close.on('click', function () {
      $el.fadeOut(function () {
        $el.remove();
      });
    })
  };
};