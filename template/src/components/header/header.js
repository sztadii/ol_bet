var header;

header = new function () {

  //catch DOM
  var $el;
  var $input;
  var $icon;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".header");
    $input = $el.find('.header__input');
    $icon = $el.find('.header__icon.-search');

    watchIcon();
  };

  var watchIcon = function () {
    $icon.on('click', function () {
      $input.addClass('-show');
    })
  };
};